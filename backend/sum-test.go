package main

import "testing"

func TestParseXY(t *testing.T) {

	tables := []struct {
		x       string
		y       string
		xExpect int
		yExpect int
	}{
		{"1", "2", 1, 2},
		{"1", "2", 1, 2},
	}

	for _, table := range tables {

		xP, yP := parseXY(table.x, table.y)

		if xP != table.xExpect && yP != table.yExpect {
			t.Errorf("Error %d %d -- %d %d", xP, yP, table.xExpect, table.yExpect)
		}

	}
}