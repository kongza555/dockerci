package main

import (
	"net/http"
	"strconv"

	"github.com/labstack/echo"
)

//--> http://localhost:5000/sum?x=2&y=20
func sum(c echo.Context) error {
	x := c.FormValue("x")
	y := c.FormValue("y")

	xValue, yValue := parseXY(x, y)
	sumValue := sumXY(xValue, yValue)

	sumString := strconv.Itoa(sumValue)
	return c.String(http.StatusOK, sumString)
}

func parseXY(x string, y string) (int, int) {
	xValue, err := strconv.Atoi(x)
	if err != nil {
		return 0, 0
	}

	yValue, err := strconv.Atoi(y)
	if err != nil {
		return xValue, 0
	}

	return xValue, yValue
}

func sumXY(x int, y int) int {
	return x + y
}